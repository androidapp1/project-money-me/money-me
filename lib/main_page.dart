import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:money_me/chart_information.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:pie_chart/pie_chart.dart';

import 'chart2_information.dart';
import 'chart3_information copy.dart';

class MainPageWidget extends StatefulWidget {
  MainPageWidget({Key? key}) : super(key: key);

  @override
  _MainPageWidgetState createState() => _MainPageWidgetState();
}

class _MainPageWidgetState extends State<MainPageWidget> {
  String? _imageUrl;
  static String name = '-';
  String gender = '-';
  int age = 0;

  @override
  void initState() {
    print(FirebaseAuth.instance.currentUser);
    _imageUrl = FirebaseAuth.instance.currentUser == null ? null : FirebaseAuth.instance.currentUser!.photoURL ;
    // print(FirebaseAuth.instance.currentUser!.photoURL);
    super.initState();
    _loadProfile();
  }
  Future<void> _loadProfile() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      name = FirebaseAuth.instance.currentUser!.displayName ?? '';
      gender = prefs.getString('gender') ?? 'Male';
      age = prefs.getInt('age') ?? 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          Card(
            clipBehavior: Clip.antiAlias,
            child: Column(
              children: [
                ListTile(
                  leading: _imageUrl == null 
                    ? const Icon(Icons.person_pin_circle_sharp, color: Colors.cyan, size: 50,)
                    : Image.network(_imageUrl!),
                  title: Text(name, style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  ),),
                  subtitle: Text('Gender: $gender\nAge: $age Years', style: TextStyle(
                    fontSize: 15,
                    fontStyle: FontStyle.italic
                  ),),
                ),
              ],
            ),
          ),
          ChartInformation(),
          Chart2Information(),
          Chart3Information(),
        ],
      ),
    );
  }
}