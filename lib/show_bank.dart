import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:money_me/bookbank_information.dart';
import 'package:money_me/bookbank_form.dart';

import 'colors.dart';

class BookbankFireStore extends StatefulWidget {
  BookbankFireStore({Key? key}) : super(key: key);

  @override
  _BookbankFireStoreState createState() => _BookbankFireStoreState();
}

class _BookbankFireStoreState extends State<BookbankFireStore> {
  final Future<FirebaseApp> _initialization = Firebase.initializeApp();
  
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(future: _initialization ,builder: (context, snapshot){
      if(snapshot.hasError){
        return Text('Error');
      }else if(snapshot.connectionState == ConnectionState.done){
        return ShowBanks();
      }else{
        return Text('Loading');
      }
    });
  }
}

class ShowBanks extends StatefulWidget {
  ShowBanks({Key? key}) : super(key: key);

  @override
  _ShowBanksState createState() => _ShowBanksState();
}

class _ShowBanksState extends State<ShowBanks> {
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Bookbank Lists'),
        backgroundColor: cyan,
      ),
      body: Center(
        child: BookbankInformation(),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: cyan,
        child: Icon(Icons.add),
        tooltip: 'Add Bank',
        onPressed: () async {
          await Navigator.push(context, MaterialPageRoute(builder: (context) => BookbankForm(bankId: '',)));
        },
      )
    );
  }
}