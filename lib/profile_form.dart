import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'colors.dart';

class ProfileWidget extends StatefulWidget {
  ProfileWidget({Key? key}) : super(key: key);

  @override
  _ProfileWidgetState createState() => _ProfileWidgetState();
}

class _ProfileWidgetState extends State<ProfileWidget> {
  final _formKey = GlobalKey<FormState>();
  String name = '';
  String gender = 'Male'; // M/F
  int age = 0;

  final _nameController = TextEditingController();
  final _ageController = TextEditingController();

  @override
  void initState(){
    super.initState();
    _loadProfile();
  }

  Future<UserCredential> signInWithGoogle() async {
    // Create a new provider
    GoogleAuthProvider googleProvider = GoogleAuthProvider();

    googleProvider
        .addScope('https://www.googleapis.com/auth/contacts.readonly');
    googleProvider.setCustomParameters({'login_hint': 'user@example.com'});

    // Once signed in, return the UserCredential
    return await FirebaseAuth.instance.signInWithPopup(googleProvider);

    // Or use signInWithRedirect
    // return await FirebaseAuth.instance.signInWithRedirect(googleProvider);
  }

  Future<void> _loadProfile() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      name = FirebaseAuth.instance.currentUser!.displayName ?? '';
      // name = FirebaseAuth.instance.currentUser!.displayName ?? prefs.getString('name') ?? '';
      _nameController.text = name;
      gender = prefs.getString('gender') ?? 'Male';
      age = prefs.getInt('age') ?? 0;
      _ageController.text = '$age';
    });
  }

  Future<void> _saveProfile() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setString('gender',gender);
      prefs.setInt('age',age);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Edit Profile'), backgroundColor: cyan,),
      body: Form(
        key: _formKey,
        child: Container(
          padding: EdgeInsets.all(16),
          child: ListView(
            children: [
              TextFormField(
                enabled: false,
                controller: _nameController,
                validator: (val){
                  if(val == null || val.isEmpty || val.length < 5){
                    return 'Please enter name';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(
                  labelText: 'Name - Lastname',
                ),
                onChanged: (val){
                  setState(() {
                    name = val;
                  });
                },
              ),
              TextFormField(
                autofocus: true,
                controller: _ageController,
                validator: (val){
                  var num = int.tryParse(val!);
                  if(num == null || num <= 0){
                    return 'Please enter more than or equal 0';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(
                  labelText: 'Age',
                ),
                onChanged: (val){
                  setState(() {
                    age = int.tryParse(val)!;
                  });
                },
                keyboardType: TextInputType.number,
              ),
              DropdownButtonFormField(
                value: gender,
                items: [
                  DropdownMenuItem(child: Row(
                    children: [
                      Icon(Icons.male),
                      SizedBox(width: 8,),
                      Text('Male'), 
                    ])
                    ,value: 'Male'
                  ),
                  DropdownMenuItem(child: Row(
                    children: [
                      Icon(Icons.female),
                      SizedBox(width: 8,),
                      Text('Female'), 
                    ])
                    ,value: 'Female'
                  ),
                ],
                onChanged: (String? newVal){
                  setState(() {
                    gender = newVal!;
                  });
                },
              ),
              SizedBox(child: Text(''),),
              ElevatedButton(
                onPressed: (){
                  if(_formKey.currentState!.validate()){
                    _saveProfile();
                    Navigator.pop(context);
                  }
                },
                child: const Text('Save'),
                style: ElevatedButton.styleFrom(
                  primary: cyan,
                  padding: EdgeInsets.all(16)
                ),
              ),
              SizedBox(child: Text(''),),
              ElevatedButton(
                onPressed: () async {
                  await signInWithGoogle();
                  // else{
                  //   await signInWithGoogleNative();
                  // }
                  Navigator.pop(context);
                },
                child: Text('Sign in with Google'),
                style: ElevatedButton.styleFrom(
                  primary: Colors.green[700],
                  padding: EdgeInsets.all(16)
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}