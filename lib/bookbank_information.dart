import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:money_me/bookbank_form.dart';

class BookbankInformation extends StatefulWidget {
  BookbankInformation({Key? key}) : super(key: key);

  @override
  _BookbankInformationState createState() => _BookbankInformationState();
}

class _BookbankInformationState extends State<BookbankInformation> {
  final Stream<QuerySnapshot> _bookbankStream = FirebaseFirestore.instance
  .collection('bookbanks')
  // .where('email',isEqualTo: FirebaseAuth.instance.currentUser!.email)
  .snapshots();

  CollectionReference bookbanks = FirebaseFirestore.instance.collection('bookbanks');
  Future<void> delBank(userId){
    return bookbanks.doc(userId).delete().then((value) => print('Bank Delete')).catchError((error) => print('Failed to delete list: $error'));
  }

  
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(stream: _bookbankStream, builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot){
      if(snapshot.hasError){
        return Text('Something went wrong');
      }
      if(snapshot.connectionState == ConnectionState.waiting){
        return Text('Loading');
      }
      return ListView(
        children: snapshot.data!.docs.map((DocumentSnapshot document) {
          Map<String, dynamic> data = document.data()! as Map<String, dynamic>;
          // print(data);
          return ListTile(
            title: Text(data['name']),
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context) => BookbankForm(bankId: document.id,)));
            },
            trailing: IconButton(onPressed: () async {
              await delBank(document.id);
            },
            icon: Icon(Icons.delete)
            ),
          );
        }).toList(),
      );
    });
  }
}