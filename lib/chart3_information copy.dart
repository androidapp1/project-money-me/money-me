import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:pie_chart/pie_chart.dart';

import 'colors.dart';

class Chart3Information extends StatefulWidget {
  Chart3Information({Key? key}) : super(key: key);

  @override
  _Chart3InformationState createState() => _Chart3InformationState();
}

class _Chart3InformationState extends State<Chart3Information> {
  double revenue = 0;
  double expense = 0;
  double saving = 0;

  final Stream<QuerySnapshot> _moneyListskStream = FirebaseFirestore.instance
  .collection('moneyLists')
  .where('bookbank',isEqualTo: 'scb')
  .snapshots();

  Map<String, double> _loadDataMap()  {
    Map<String, double> dataMap = {
      'Revenue': revenue,
      'Expense': expense,
      'Saving' : saving
      };
    return dataMap;
  }

  List<Color> colorlist = [
    scb,
    redAccent,
    yellow
  ];

  @override
  Widget build(BuildContext context) {
    // print(getData(bookbanks));
    // print(getMoneyLists('ktb'));
    return StreamBuilder(stream: _moneyListskStream, builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot){
      if(snapshot.hasError){
        return Text('Something went wrong');
      }
      if(snapshot.connectionState == ConnectionState.waiting){
        return Text('Loading');
      }
      snapshot.data!.docs.map((DocumentSnapshot document){
        Map<String, dynamic> data = document.data()! as Map<String, dynamic>;
        if(data['type'] == 'expense'){
          expense += data['money'];
        }else if(data['type'] == 'revenue'){
          revenue += data['money'];
        }else{
          saving += data['money'];
        }
        print(data['money']);
      }).toList();
      return Column(
        children: [
          SizedBox(child: Text(''),),
          Card(
          clipBehavior: Clip.antiAlias,
          child: Column(
            children: [
              Text('ธนาคารไทยพาณิชย์',
                style: TextStyle(
                  fontSize: 20,
                  color: scb,
                  fontWeight: FontWeight.bold
                ),
              ),
              PieChart(
                dataMap: _loadDataMap(),
                chartLegendSpacing: 50,
                colorList: colorlist,
                chartRadius: MediaQuery.of(context).size.width / 3.5,
              )
            ],
          ),
        )
        ],
      );
    });
  }
}