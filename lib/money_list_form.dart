import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'colors.dart';

// ignore: must_be_immutable
class MoneyListForm extends StatefulWidget {
  String listId;
  MoneyListForm({Key? key, required this.listId}) : super(key: key);

  @override
  _MoneyListFormState createState() => _MoneyListFormState(this.listId);
}

class _MoneyListFormState extends State<MoneyListForm> {
  String listId;
  _MoneyListFormState(this.listId);

  String bookbank = 'ktb';
  DateTime current = DateTime.now();
  late Timestamp date;
  String money = '0';
  String title = '';
  String type = 'revenue';

  final _formKey = GlobalKey<FormState>();

  CollectionReference moneyLists = FirebaseFirestore.instance.collection('moneyLists');
  final Stream<QuerySnapshot> _bookbankStream = FirebaseFirestore.instance
  .collection('bookbanks')
  .snapshots();

  TextEditingController _bookbankController = new TextEditingController();
  // TextEditingController _dateController = new TextEditingController();
  TextEditingController _moneyController = new TextEditingController();
  TextEditingController _titleController = new TextEditingController();
  TextEditingController _typeController = new TextEditingController();
  
  @override
  void initState() {
    super.initState();
    if(this.listId.isNotEmpty){
      moneyLists.doc(this.listId).get().then((snapshot){
        if(snapshot.exists){
          var data = snapshot.data() as Map<String, dynamic>;
          // DateTime dateRaw = DateTime.parse(data['date'].toDate().toString());
          // String date = DateFormat.yMMMd().format(dateRaw);
          setState(() {
            bookbank = data['bookbank'];
            // date = data['date'];
            // print(date);
            money = data['money'].toString();
            title = data['title'];
            type = data['type'];
            _bookbankController.text = bookbank;
            // _dateController.text = date;
            _moneyController.text = money;
            _titleController.text = title;
            _typeController.text = type;
          });
        }
      });
    }
  }

  Future<void> addList(){
    return moneyLists.add({
      'bookbank': this.bookbank,
      'date': this.date,
      'money': double.parse(this.money),
      'title': this.title,
      'type': this.type,
      'email': FirebaseAuth.instance.currentUser!.email
    })
    .then((value) => print('List added'))
    .catchError((error) => print('Failed to add list: $error'));
  }

  Future<void> updateList() {
    return moneyLists
      .doc(this.listId)
      .update({
        'bookbank': this.bookbank,
        'date': this.date,
        'money': double.parse(this.money),
        'title': this.title,
        'type': this.type})
      .then((value) => print("List Updated"))
      .catchError((error) => print("Failed to update list: $error"));
  }

  @override
  Widget build(BuildContext context) {
    final format = DateFormat("yyyy-MM-dd");
    return Scaffold(
      appBar: AppBar(title: Text('Money List Form'), backgroundColor: cyan),
      body: Form(
        key: _formKey,
        child: Container(
          padding: EdgeInsets.all(16),
          child: ListView(
            children: [
              DateTimeField(
                decoration: InputDecoration(
                  labelText: 'Date'
                ),
                format: format,
                onShowPicker: (context, currentValue) {
                  return showDatePicker(
                      context: context,
                      firstDate: DateTime(1900),
                      initialDate: currentValue ?? DateTime.now(),
                      lastDate: DateTime(2100));
                },
                onChanged: (val){
                  Timestamp val2 = Timestamp.fromDate(val!);
                  date = val2;
                },
              ),
              TextFormField(
                autofocus: true,
                controller: _titleController,
                validator: (val){
                  if(val == null || val.isEmpty){
                    return 'Please enter title';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(
                  labelText: 'Title',
                ),
                onChanged: (val){
                  setState(() {
                    title = val;
                  });
                },
              ),
              DropdownButtonFormField(
                value: type,
                items: [
                  DropdownMenuItem(child: Row(
                    children: [
                      Icon(Icons.add_circle),
                      SizedBox(width: 8,),
                      Text('รายรับ'), 
                    ])
                    ,value: 'revenue'
                  ),
                  DropdownMenuItem(child: Row(
                    children: [
                      Icon(Icons.remove_circle),
                      SizedBox(width: 8,),
                      Text('รายจ่าย'), 
                    ])
                    ,value: 'expense'
                  ),
                  DropdownMenuItem(child: Row(
                    children: [
                      Icon(Icons.local_atm_rounded),
                      SizedBox(width: 8,),
                      Text('เงินเก็บ'), 
                    ])
                    ,value: 'saving'
                  ),
                ],
                onChanged: (String? val){
                  setState(() {
                    type = val!;
                  });
                },
              ),
              TextFormField(
                controller: _moneyController,
                validator: (val){
                  var num = double.tryParse(val!);
                  if(num == null || num <= 0){
                    return 'Please enter more than or equal 0';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(
                  labelText: 'Money',
                ),
                onChanged: (val){
                  setState(() {
                    money = val;
                  });
                },
                keyboardType: TextInputType.number,
              ),
              StreamBuilder(stream: _bookbankStream, builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot){
                if(snapshot.hasError){
                  return Text('Something went wrong');
                }
                if(snapshot.connectionState == ConnectionState.waiting){
                  return Text('Loading');
                }
                return new DropdownButtonFormField(
                  value: bookbank,
                  items: snapshot.data!.docs.map((DocumentSnapshot document){
                    Map<String, dynamic> data = document.data()! as Map<String, dynamic>;
                    return new DropdownMenuItem(
                      child: Text(data['name']),
                      value: data['key'],
                    );
                  }).toList(),
                  onChanged: (val){
                    setState(() {
                      bookbank = val.toString();
                    });
                  },
                  );
              },),
              SizedBox(child: Text(''),),
              ElevatedButton(
                onPressed: () async {
                  if(_formKey.currentState!.validate()){
                    if(listId.isEmpty){
                    await addList();
                  }else{
                    await updateList();
                  }
                  Navigator.pop(context);
                  }
                },
                child: const Text('Save'),
                style: ElevatedButton.styleFrom(
                  primary: Colors.cyan,
                  padding: EdgeInsets.all(16)
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}