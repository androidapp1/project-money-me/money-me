import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'colors.dart';

class BookbankForm extends StatefulWidget {
  String bankId;
  BookbankForm({Key? key, required this.bankId}) : super(key: key);

  @override
  _BookbankFormState createState() => _BookbankFormState(this.bankId);
}

class _BookbankFormState extends State<BookbankForm> {
  String bankId;
  _BookbankFormState(this.bankId);

  String name = '';
  String key = '';

  final _formKey = GlobalKey<FormState>();

  CollectionReference bookbanks = FirebaseFirestore.instance.collection('bookbanks');
  TextEditingController _nameController = new TextEditingController();
  TextEditingController _keyController = new TextEditingController();

  @override
  void initState() {
    super.initState();
    if(this.bankId.isNotEmpty){
      bookbanks.doc(this.bankId).get().then((snapshot){
        if(snapshot.exists){
          var data = snapshot.data() as Map<String, dynamic>;
          setState(() {
            name = data['name'];
            key = data['key'];
            _nameController.text = name;
            _keyController.text = key;
          });
        }
      });
    }
  }

  Future<void> addBank(){
    return bookbanks.add({
      'name': this.name,
      'key': this.key,
      'email': FirebaseAuth.instance.currentUser!.email
    })
    .then((value) => print('Bank added'))
    .catchError((error) => print('Failed to add bank: $error'));
  }

  Future<void> updateBank() {
    return bookbanks
      .doc(this.bankId)
      .update({
        'name': this.name,
        'key': this.key,
        })
      .then((value) => print("Bank Updated"))
      .catchError((error) => print("Failed to update bamk: $error"));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Bookbank Form'), backgroundColor: cyan),
      body: Form(
        key: _formKey,
        child: Container(
          padding: EdgeInsets.all(16),
          child: ListView(
            children: [
              TextFormField(
                autofocus: true,
                controller: _nameController,
                validator: (val){
                  if(val == null || val.isEmpty){
                    return 'Please enter bank name';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(
                  labelText: 'Bank Name',
                ),
                onChanged: (val){
                  setState(() {
                    name = val;
                  });
                },
              ),
              TextFormField(
                controller: _keyController,
                validator: (val){
                  if(val == null || val.isEmpty){
                    return 'Please enter bank acronym';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(
                  labelText: 'Bank English Acronym Name',
                ),
                onChanged: (val){
                  setState(() {
                    key = val;
                  });
                },
              ),
              SizedBox(child: Text(''),),
              ElevatedButton(
                onPressed: () async {
                  if(_formKey.currentState!.validate()){
                    if(bankId.isEmpty){
                    await addBank();
                  }else{
                    await updateBank();
                  }
                  Navigator.pop(context);
                  }
                },
                child: const Text('Save'),
                style: ElevatedButton.styleFrom(
                  primary: cyan,
                  padding: EdgeInsets.all(16)
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}