import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'colors.dart';
import 'money_list_form.dart';

class MoneyListInformation extends StatefulWidget {
  MoneyListInformation({Key? key}) : super(key: key);

  @override
  _MoneyListInformationState createState() => _MoneyListInformationState();
}

class _MoneyListInformationState extends State<MoneyListInformation> {
  final Stream<QuerySnapshot> _moneyListStream = FirebaseFirestore.instance
  .collection('moneyLists')
  // .where('email',isEqualTo: FirebaseAuth.instance.currentUser!.email)
  .orderBy('date', descending: true)
  .snapshots();

  CollectionReference moneyLists = FirebaseFirestore.instance.collection('moneyLists');
  Future<void> delList(userId){
    return moneyLists.doc(userId).delete().then((value) => print('List Delete')).catchError((error) => print('Failed to delete list: $error'));
  }
  
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(stream: _moneyListStream, builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot){
      if(snapshot.hasError){
        return Text('Something went wrong');
      }
      if(snapshot.connectionState == ConnectionState.waiting){
        return Text('Loading');
      }
      return ListView(
        children: snapshot.data!.docs.map((DocumentSnapshot document) {
          Map<String, dynamic> data = document.data()! as Map<String, dynamic>;
          // print(data);
          DateTime dateRaw = DateTime.parse(data['date'].toDate().toString());
          String date = DateFormat.yMMMd().format(dateRaw);
          return ListTile(
            title: Text(data['title']+' '+data['money'].toString()+' บาท', 
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: data['type'] == 'expense' ? Colors.red[900] : Colors.green[800]
              )
            ),
            subtitle: Text(data['bookbank']+' '+date.toString()),
            tileColor: data['type'] =='expense' ? red : green,
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context) => MoneyListForm(listId: document.id,)));
            },
            trailing: IconButton(onPressed: () async {
              await delList(document.id);
            },
            icon: Icon(Icons.delete)
            ),
          );
        }).toList(),
      );
    });
  }
}