import 'package:flutter/material.dart';

Color green = Colors.teal.shade50;
Color red = Colors.red.shade100;
Color cyan = Colors.cyan;
Color redAccent = Colors.redAccent;
Color yellow = Colors.amberAccent;
Color ktb = Colors.lightBlue.shade600;
Color kbank = Colors.teal.shade400;
Color scb = Colors.purple.shade600;