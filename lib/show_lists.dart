import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'colors.dart';
import 'money_list_form.dart';
import 'money_list_information.dart';

class MoneyListFirestore extends StatefulWidget {
  MoneyListFirestore({Key? key}) : super(key: key);

  @override
  _MoneyListFirestoreState createState() => _MoneyListFirestoreState();
}

class _MoneyListFirestoreState extends State<MoneyListFirestore> {
  final Future<FirebaseApp> _initialization = Firebase.initializeApp();
  
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(future: _initialization ,builder: (context, snapshot){
      if(snapshot.hasError){
        return Text('Error');
      }else if(snapshot.connectionState == ConnectionState.done){
        return ShowLists();
      }else{
        return Text('Loading');
      }
    });
  }
}

class ShowLists extends StatefulWidget {
  ShowLists({Key? key}) : super(key: key);

  @override
  _ShowListsState createState() => _ShowListsState();
}

class _ShowListsState extends State<ShowLists> {
  // late Future<List<MoneyList>> _moneyLists;

  // @override
  // void initState() {
  //   super.initState();
  //   _moneyLists = getMoneyList();
  // }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: MoneyListInformation(),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: cyan,
        child: Icon(Icons.add),
        tooltip: 'Add List',
        onPressed: () async {
          await Navigator.push(context, MaterialPageRoute(builder: (context) => MoneyListForm(listId: '',)));
        },
      ),
    );
  }
}