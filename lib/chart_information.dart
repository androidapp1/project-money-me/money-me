import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:pie_chart/pie_chart.dart';

import 'colors.dart';

class ChartInformation extends StatefulWidget {
  ChartInformation({Key? key}) : super(key: key);

  @override
  _ChartInformationState createState() => _ChartInformationState();
}

class _ChartInformationState extends State<ChartInformation> {
  double revenue = 0;
  double expense = 0;
  double saving = 0;
  final Stream<QuerySnapshot> _bookbankStream = FirebaseFirestore.instance
  .collection('bookbanks')
  .snapshots();

  final Stream<QuerySnapshot> _moneyListskStream = FirebaseFirestore.instance
  .collection('moneyLists')
  .where('bookbank',isEqualTo: 'ktb')
  .snapshots();

  CollectionReference bookbanks =
    FirebaseFirestore.instance.collection('bookbanks');

  Query<Map<String, dynamic>> moneyLists =
    FirebaseFirestore.instance.collection('moneyLists').where('bookbank',isEqualTo: 'ktb');

  Future<void> getData(collectionRef) async {
      // Get docs from collection reference
    QuerySnapshot querySnapshot = await collectionRef.get();
      // Get data from docs and convert map to List
    final allData = querySnapshot.docs.map((doc) => doc.data()).toList();
    print(allData);
    // for (int i = 0; i < querySnapshot.docs.length; i++) {
    //   var a = querySnapshot.docs[i];
    //   print(a.data());
    // }
  }

  Future<void> getMoneyLists(bookbank) async {
      // Get docs from collection reference
    QuerySnapshot querySnapshot = await FirebaseFirestore.instance.collection('moneyLists').where('bookbank',isEqualTo: bookbank).get();
      // Get data from docs and convert map to List
    final allData = querySnapshot.docs.map((doc) => doc.data()).toList();
    print(allData);
    // for (int i = 0; i < querySnapshot.docs.length; i++) {
      // var a = querySnapshot.docs[i];
      // print(a.data());
    // }
  }
  
  Stream<QuerySnapshot> getListsByBookbank(bookbank) {
    return FirebaseFirestore.instance
        .collection('moneyLists')
        .where('bookbank', isEqualTo: bookbank)
        .snapshots();
  }

  Map<String, double> _loadDataMap()  {
    Map<String, double> dataMap = {
      'Revenue': revenue,
      'Expense': expense,
      'Saving' : saving
      };
    return dataMap;
  }

  List<Color> colorlist = [
    ktb,
    redAccent,
    yellow
  ];

  @override
  Widget build(BuildContext context) {
    // print(getData(bookbanks));
    // print(getMoneyLists('ktb'));
    return StreamBuilder(stream: _moneyListskStream, builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot){
      if(snapshot.hasError){
        return Text('Something went wrong');
      }
      if(snapshot.connectionState == ConnectionState.waiting){
        return Text('Loading');
      }
      snapshot.data!.docs.map((DocumentSnapshot document){
        Map<String, dynamic> data = document.data()! as Map<String, dynamic>;
        if(data['type'] == 'expense'){
          expense += data['money'];
        }else if(data['type'] == 'revenue'){
          revenue += data['money'];
        }else{
          saving += data['money'];
        }
        print(data['money']);
      }).toList();
      return Column(
        children: [
          SizedBox(child: Text(''),),
          Card(
          clipBehavior: Clip.antiAlias,
          child: Column(
            children: [
              Text('ธนาคารกรุงไทย',
                style: TextStyle(
                  fontSize: 20,
                  color: ktb,
                  fontWeight: FontWeight.bold
                ),
              ),
              PieChart(
                dataMap: _loadDataMap(),
                chartLegendSpacing: 50,
                colorList: colorlist,
                chartRadius: MediaQuery.of(context).size.width / 3.5,
              )
            ],
          ),
        )
        ],
      );
    });
  }
}