import 'package:flutter/material.dart';
import 'package:money_me/show_bank.dart';

import 'profile_form.dart';

class ProfileMenuWidget extends StatefulWidget {
  ProfileMenuWidget({Key? key}) : super(key: key);

  @override
  _ProfileMenuWidgetState createState() => _ProfileMenuWidgetState();
}

class _ProfileMenuWidgetState extends State<ProfileMenuWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          ListTile(
            leading: Icon(Icons.edit),
            title: Text('Edit Profile'),
            onTap: () async {
              await Navigator.push(context, MaterialPageRoute(builder: (context) => ProfileWidget()));
            },
          ),
          ListTile(
            leading: Icon(Icons.edit),
            title: Text('Edit Bookbank'),
            onTap: () async {
              await Navigator.push(context, MaterialPageRoute(builder: (context) => ShowBanks()));
            },
          ),
        ],
      )
    );
  }
}